/*
* MATLAB Compiler: 6.5 (R2017b)
* Date: Wed Jun 13 23:50:25 2018
* Arguments:
* "-B""macro_default""-W""dotnet:Mathworks.Training.Fuzzy,Calculo,4.0,private""-T""link:li
* b""-d""C:\Users\Edison\Documents\SyncGoogleDrive\CICLO VII\Inteligencia
* Artificial\Proyecto\expertoDifuso-nutricional\Utilitarios_difuso\IMC\for_testing""-v""cl
* ass{Calculo:C:\Users\Edison\Documents\SyncGoogleDrive\CICLO VII\Inteligencia
* Artificial\Proyecto\expertoDifuso-nutricional\Utilitarios_difuso\IMC.m}""-a""C:\Users\Ed
* ison\Documents\SyncGoogleDrive\CICLO VII\Inteligencia
* Artificial\Proyecto\expertoDifuso-nutricional\Utilitarios_difuso\IMC_2018.fis"
*/
using System;
using System.Reflection;
using System.IO;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;

#if SHARED
[assembly: System.Reflection.AssemblyKeyFile(@"")]
#endif

namespace Mathworks.Training.FuzzyNative
{

  /// <summary>
  /// The Calculo class provides a CLS compliant, Object (native) interface to the MATLAB
  /// functions contained in the files:
  /// <newpara></newpara>
  /// C:\Users\Edison\Documents\SyncGoogleDrive\CICLO VII\Inteligencia
  /// Artificial\Proyecto\expertoDifuso-nutricional\Utilitarios_difuso\IMC.m
  /// </summary>
  /// <remarks>
  /// @Version 4.0
  /// </remarks>
  public class Calculo : IDisposable
  {
    #region Constructors

    /// <summary internal= "true">
    /// The static constructor instantiates and initializes the MATLAB Runtime instance.
    /// </summary>
    static Calculo()
    {
      if (MWMCR.MCRAppInitialized)
      {
        try
        {
          Assembly assembly= Assembly.GetExecutingAssembly();

          string ctfFilePath= assembly.Location;

          int lastDelimiter= ctfFilePath.LastIndexOf(@"\");

          ctfFilePath= ctfFilePath.Remove(lastDelimiter, (ctfFilePath.Length - lastDelimiter));

          string ctfFileName = "Fuzzy.ctf";

          Stream embeddedCtfStream = null;

          String[] resourceStrings = assembly.GetManifestResourceNames();

          foreach (String name in resourceStrings)
          {
            if (name.Contains(ctfFileName))
            {
              embeddedCtfStream = assembly.GetManifestResourceStream(name);
              break;
            }
          }
          mcr= new MWMCR("",
                         ctfFilePath, embeddedCtfStream, true);
        }
        catch(Exception ex)
        {
          ex_ = new Exception("MWArray assembly failed to be initialized", ex);
        }
      }
      else
      {
        ex_ = new ApplicationException("MWArray assembly could not be initialized");
      }
    }


    /// <summary>
    /// Constructs a new instance of the Calculo class.
    /// </summary>
    public Calculo()
    {
      if(ex_ != null)
      {
        throw ex_;
      }
    }


    #endregion Constructors

    #region Finalize

    /// <summary internal= "true">
    /// Class destructor called by the CLR garbage collector.
    /// </summary>
    ~Calculo()
    {
      Dispose(false);
    }


    /// <summary>
    /// Frees the native resources associated with this object
    /// </summary>
    public void Dispose()
    {
      Dispose(true);

      GC.SuppressFinalize(this);
    }


    /// <summary internal= "true">
    /// Internal dispose function
    /// </summary>
    protected virtual void Dispose(bool disposing)
    {
      if (!disposed)
      {
        disposed= true;

        if (disposing)
        {
          // Free managed resources;
        }

        // Free native resources
      }
    }


    #endregion Finalize

    #region Methods

    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the IMC MATLAB function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// display(Difuso);
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object IMC()
    {
      return mcr.EvaluateFunction("IMC", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the IMC MATLAB function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// display(Difuso);
    /// </remarks>
    /// <param name="peso">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object IMC(Object peso)
    {
      return mcr.EvaluateFunction("IMC", peso);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the IMC MATLAB function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// display(Difuso);
    /// </remarks>
    /// <param name="peso">Input argument #1</param>
    /// <param name="talla">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object IMC(Object peso, Object talla)
    {
      return mcr.EvaluateFunction("IMC", peso, talla);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the IMC MATLAB function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// display(Difuso);
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] IMC(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "IMC", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the IMC MATLAB function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// display(Difuso);
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="peso">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] IMC(int numArgsOut, Object peso)
    {
      return mcr.EvaluateFunction(numArgsOut, "IMC", peso);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the IMC MATLAB function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// display(Difuso);
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="peso">Input argument #1</param>
    /// <param name="talla">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] IMC(int numArgsOut, Object peso, Object talla)
    {
      return mcr.EvaluateFunction(numArgsOut, "IMC", peso, talla);
    }


    /// <summary>
    /// Provides an interface for the IMC function in which the input and output
    /// arguments are specified as an array of Objects.
    /// </summary>
    /// <remarks>
    /// This method will allocate and return by reference the output argument
    /// array.<newpara></newpara>
    /// M-Documentation:
    /// display(Difuso);
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return</param>
    /// <param name= "argsOut">Array of Object output arguments</param>
    /// <param name= "argsIn">Array of Object input arguments</param>
    /// <param name= "varArgsIn">Array of Object representing variable input
    /// arguments</param>
    ///
    [MATLABSignature("IMC", 2, 1, 0)]
    protected void IMC(int numArgsOut, ref Object[] argsOut, Object[] argsIn, params Object[] varArgsIn)
    {
        mcr.EvaluateFunctionForTypeSafeCall("IMC", numArgsOut, ref argsOut, argsIn, varArgsIn);
    }

    /// <summary>
    /// This method will cause a MATLAB figure window to behave as a modal dialog box.
    /// The method will not return until all the figure windows associated with this
    /// component have been closed.
    /// </summary>
    /// <remarks>
    /// An application should only call this method when required to keep the
    /// MATLAB figure window from disappearing.  Other techniques, such as calling
    /// Console.ReadLine() from the application should be considered where
    /// possible.</remarks>
    ///
    public void WaitForFiguresToDie()
    {
      mcr.WaitForFiguresToDie();
    }



    #endregion Methods

    #region Class Members

    private static MWMCR mcr= null;

    private static Exception ex_= null;

    private bool disposed= false;

    #endregion Class Members
  }
}

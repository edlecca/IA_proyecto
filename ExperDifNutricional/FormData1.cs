﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using  Mathworks.Training.Fuzzy;   //Parte difusa(Mathlab)
using MathWorks.MATLAB.NET.Arrays; //
using Prolog;
//using SbsSW.SwiPlCs;//Parte experta(Swi-Prolog)

namespace ExperDifNutricional
{

    public partial class FormData1 : Form
    {
        public FormData1()
        {
            InitializeComponent();
        }
        //Prolog
        
        Calculo indice = null;
        MWNumericArray resul = null;
        MWArray[] entradas = new MWArray[2];
        private void FormData1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
           //Parte difusa
            entradas[0] =double.Parse(textPeso.Text);
            entradas[1] =double.Parse(textAltura.Text);
            indice = new Calculo();
            resul = (MWNumericArray)indice.IMC(entradas[0], entradas[1]);
            string mostrar = resul.ToString();

           //Parte experta 
            var prolog = new PrologEngine(persistentCommandHistory: false);

            // 'socrates' is human.
           // prolog.ConsultFromString("human(socrates).");
            // human is bound to die.
           // prolog.ConsultFromString("mortal(X) :- human(X).");

            // Question: Shall 'socrates' die?
            var solution = prolog.GetFirstSolution(query: "mortal(socrates).");
           // Console.WriteLine(solution.Solved); // = "True" (Yes!)

            //Logica proveniente de la interfaz
            if (rbtn_masculino.Checked.Equals(true))
            {
                
                //txt_Mostrar.Text = "Paciente masculino con IMC de:" + mostrar + "--- ";
            }

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }


    }
}
